package my.project.protocol;

public class MessageImpl extends Request<String> {

    public MessageImpl() {
        super(null);
    }


    public MessageImpl(String username, String body) {
        super(body);
        this.setUserName(username);
    }
}
