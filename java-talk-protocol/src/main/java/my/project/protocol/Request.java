package my.project.protocol;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include =  JsonTypeInfo.As.PROPERTY, property="type"  )
@JsonSubTypes({
        @JsonSubTypes.Type(value = MessageImpl.class),
        @JsonSubTypes.Type(value = Response.class)
})
public abstract class Request<T> implements Serializable {
    private Integer id;
    private String userName;
    private T body;

    public Request() {
    }

    public Request(T body) {
        this.body = body;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Request{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", body=" + body +
                '}';
    }
}
