package my.project.protocol;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.CharMatcher;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Base64;

import static com.google.common.base.Preconditions.checkArgument;

public class TransportProtocol {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransportProtocol.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.configure(SerializationFeature.INDENT_OUTPUT, true);
    }


    public static String prepareMessageToTransfer(String message) {
        if (message == null || message.isEmpty()) {
            return null;
        }
        LOGGER.debug("prepareMessageToTransfer " + message);

        String encoded = toBase64(message);

        LOGGER.debug("prepareMessageToTransfer result " + encoded);

        return "{" + encoded + "}";
    }

    private static String toBase64(String message) {
        return Base64.getEncoder().encodeToString(message.getBytes());
    }

    private static byte[] fromBase64(String message) {
        return Base64.getDecoder().decode(message);
    }

    public static String restoreMessage(String message) {
        checkArgument(!Strings.isNullOrEmpty(message));
        checkArgument(message.startsWith("{"));
        checkArgument(message.endsWith("}"));

        LOGGER.debug("restoreMessage " + message);
        String s =  CharMatcher.anyOf("{}").replaceFrom(message.trim(), "");

        try {
            message = new String(fromBase64(s));
        } catch (Exception e){
            LOGGER.error(message, e);
            throw new IllegalArgumentException(e);
        }

        LOGGER.debug("restoreMessage result " + message);

        return message;
    }

    public static String prepareMessageToTransfer(Request message) {
        try {
            return prepareMessageToTransfer(unmarshall(message));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Request marshall(String request) throws IOException {
        return MAPPER.readValue(request, Request.class);
    }

    public static String unmarshall(Request request) throws JsonProcessingException {
        return MAPPER.writeValueAsString(request);
    }
}
