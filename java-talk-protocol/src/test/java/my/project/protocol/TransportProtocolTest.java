package my.project.protocol;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class TransportProtocolTest {
    private static final String testString = "Test string";
    private static final String encodedString = "{ewogICJ0eXBlIiA6ICJNZXNzYWdlSW1wbCIsCiAgImlkIiA6IDEsCiAgInVzZXJOYW1lIiA6ICJjbGllbnQ0MyIsCiAgImJvZHkiIDogImNsaWVudDQzIgp9}";

    @Test
    public void prepared_message_should_be_start_with_brace() {
        String prepared = TransportProtocol.prepareMessageToTransfer(testString);
        assertTrue(prepared.startsWith("{"));

        Request request = new MessageImpl(testString, testString);
        prepared = TransportProtocol.prepareMessageToTransfer(request);
        assertTrue(prepared.startsWith("{"));
    }

    @Test
    public void prepared_message_should_be_end_with_brace() {
        String prepared = TransportProtocol.prepareMessageToTransfer(testString);
        assertTrue(prepared.endsWith("}"));

        Request request = new MessageImpl(testString, testString);
        prepared = TransportProtocol.prepareMessageToTransfer(request);
        assertTrue(prepared.endsWith("}"));
    }

    @Test
    public void restore_test() throws IOException {
        String msg = TransportProtocol.restoreMessage(encodedString);

        Request request = TransportProtocol.marshall(msg);

        assertTrue(request != null);
    }

    @Test
    public void encoded_string_wihtout_braces_should_be_thrown_IllegalArgumentException(){
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> {
            TransportProtocol.restoreMessage(encodedString.replace("{", ""));
        });

        thrown = assertThrows(IllegalArgumentException.class, () -> {
            TransportProtocol.restoreMessage(encodedString.replace("}", ""));
        });
    }

    @Test
    public void emptye_string_should_be_thrown_IllegalArgumentException(){
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> {
            TransportProtocol.restoreMessage("");
        });

        thrown = assertThrows(IllegalArgumentException.class, () -> {
            TransportProtocol.restoreMessage(null);
        });
    }
}