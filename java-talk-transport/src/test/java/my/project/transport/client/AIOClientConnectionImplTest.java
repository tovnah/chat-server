package my.project.transport.client;

import my.project.transport.Connection;
import my.project.transport.ConnectionTest;
import my.project.transport.impl.aio.client.AioClientConnectionImpl;

public class AIOClientConnectionImplTest extends ConnectionTest {

    @Override
    public Connection getBadConnection() {
        return new AioClientConnectionImpl(null, null);
    }
}