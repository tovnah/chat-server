package my.project.transport.client;

import my.project.transport.Connection;
import my.project.transport.ConnectionTest;
import my.project.transport.impl.nio.client.NioClientConnectionImpl;

public class NioClientConnectionImplTest extends ConnectionTest {

    @Override
    public Connection getBadConnection() {
        return new NioClientConnectionImpl(null, null);
    }
}