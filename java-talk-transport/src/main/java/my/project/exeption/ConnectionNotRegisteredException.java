package my.project.exeption;

public class ConnectionNotRegisteredException extends ServerTransportException{
    public ConnectionNotRegisteredException() {
    }

    public ConnectionNotRegisteredException(String message) {
        super(message);
    }

    public ConnectionNotRegisteredException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectionNotRegisteredException(Throwable cause) {
        super(cause);
    }
}
