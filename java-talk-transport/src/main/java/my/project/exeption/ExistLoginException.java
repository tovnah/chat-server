package my.project.exeption;

public class ExistLoginException extends ServerTransportException {
    public ExistLoginException() {
    }

    public ExistLoginException(String message) {
        super(message);
    }

    public ExistLoginException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExistLoginException(Throwable cause) {
        super(cause);
    }
}
