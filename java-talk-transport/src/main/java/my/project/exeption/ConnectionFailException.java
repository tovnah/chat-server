package my.project.exeption;

public class ConnectionFailException extends ServerTransportException {
    public ConnectionFailException() {
    }

    public ConnectionFailException(String message) {
        super(message);
    }

    public ConnectionFailException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectionFailException(Throwable cause) {
        super(cause);
    }
}
