package my.project.transport;

public class Configuration {
    public static Integer getBufferSize(){
        return Const.bufferSize;
    }

    public static String getServerAddress(){
        return Const.host;
    }

    public static int getServerPort(){
        return Const.port;
    }
}
