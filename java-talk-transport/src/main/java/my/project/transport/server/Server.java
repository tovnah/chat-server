package my.project.transport.server;

import my.project.exeption.ServerTransportException;
import my.project.transport.Connection;

import java.nio.channels.CompletionHandler;

public interface Server {
    void start();

    void registerConnection(Connection connection) throws ServerTransportException;

    void removeConnection(Connection connection) throws ServerTransportException;

    ServerRequestManager getMessageManager();
}

