package my.project.transport.server;

import my.project.protocol.Request;
import my.project.transport.Connection;

import java.util.List;

public interface ServerRequestManager {

    void acceptRequest(String request, Connection connection) throws NullPointerException;

    void broadcast(Request request, Connection connection) throws NullPointerException;

    List<Request> getLastRequests();

    Connection getConnectionByName(String name);

    void registerConnection(Connection connection) throws NullPointerException;

    void unregisterConnection(Connection connection) throws NullPointerException;

    void onlyAuthorAnswer(Request request, Connection connection) throws NullPointerException;

    void closeConnection(Connection connection);
}
