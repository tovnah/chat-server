package my.project.transport.impl.nio.server;

import com.google.common.base.Strings;
import my.project.exeption.ConnectionFailException;
import my.project.protocol.TransportProtocol;
import my.project.transport.Connection;
import my.project.transport.Const;
import my.project.transport.server.ServerRequestManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.*;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class NioServerConnectionImpl implements Connection {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private SocketChannel channel;
    private Selector selector;

    private String userName;

    private final ServerRequestManager messageManager;

    public NioServerConnectionImpl(SocketChannel channel, Selector selector, ServerRequestManager messageManager) {
        this.channel = channel;
        this.selector = selector;
        this.messageManager = messageManager;
    }

    @Override
    public void open() throws ConnectionFailException {
        try {
            channel.configureBlocking(false);
            channel.register(selector, SelectionKey.OP_READ);
        } catch (Exception e) {
            throw new ConnectionFailException(e);
        }
    }

    @Override
    public Long getId() {
        return null;
    }

    @Override
    public void close() {

    }

    @Override
    public void send(String message) throws IllegalArgumentException {
        try {
            checkArgument(!Strings.isNullOrEmpty(message));

            ByteBuffer buffer = ByteBuffer.wrap(TransportProtocol.prepareMessageToTransfer(message).getBytes());

            channel.write(buffer);

            channel.register(selector, SelectionKey.OP_READ);
        } catch (IOException e) {
            LOGGER.error("Send error", e);
        }
    }

    @Override
    public void read() {

    }

    @Override
    public boolean isClosed() {
        if (channel == null) {
            return true;
        }
        return !channel.isOpen();
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public Boolean isRegistered() {
        return !Strings.isNullOrEmpty(userName);
    }

    @Override
    public void addMessageToQueue(String request) {
        messageManager.acceptRequest(request, this);
    }

    @Override
    public void onReadFinish() {

    }

    @Override
    public void onReadStart() {

    }
}
