package my.project.transport.impl.aio.client.handlers;

import my.project.transport.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.channels.CompletionHandler;

public class ClientWriteHandler  implements CompletionHandler<Integer, ByteBuffer> {

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private final Connection connection;

    public ClientWriteHandler( Connection connection) {
        this.connection = connection;
    }

    @Override
    public void completed(Integer result, ByteBuffer inputBuffer) {
        LOGGER.debug("onWrite fired");
        inputBuffer.clear();
//       ReadServerHandler readCompletionHandler = new ReadServerHandler(socketChannel, inputBuffer, null);
//        socketChannel.read(inputBuffer, inputBuffer, readCompletionHandler);
        connection.read();
    }

    @Override
    public void failed(Throwable exc, ByteBuffer inputBuffer) {
        LOGGER.error("Write failed", exc);
    }
}
