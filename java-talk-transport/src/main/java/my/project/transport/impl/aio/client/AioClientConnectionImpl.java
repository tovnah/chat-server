package my.project.transport.impl.aio.client;

import com.google.common.base.Strings;
import my.project.exeption.ConnectionFailException;
import my.project.protocol.Request;
import my.project.protocol.Response;
import my.project.protocol.TransportProtocol;
import my.project.transport.AsyncSocketConnection;
import my.project.transport.Configuration;
import my.project.transport.Connection;
import my.project.transport.impl.aio.client.handlers.ClientReadHandler;
import my.project.transport.impl.aio.client.handlers.ClientWriteHandler;
import my.project.transport.client.ClientRequestManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import static com.google.common.base.Preconditions.checkArgument;

public class AioClientConnectionImpl implements Connection, AsyncSocketConnection {
    private Logger LOGGER = LoggerFactory.getLogger(AioClientConnectionImpl.class);

    private String userName;

    private AsynchronousSocketChannel channel;
    private InetSocketAddress hostAddress;

    private CompletionHandler<Integer, ByteBuffer> readHandler;
    private CompletionHandler<Integer, ByteBuffer> writeHandler;

    private ByteBuffer inputBuffer = ByteBuffer.allocate(Configuration.getBufferSize());
    private ByteBuffer outputBuffer = ByteBuffer.allocate(Configuration.getBufferSize());

    private ClientRequestManager clientRequestManager;

    private ReentrantLock locker;
    private Condition condition;

    public AioClientConnectionImpl(InetSocketAddress hostAddress, ClientRequestManager clientRequestManager) {
        this.hostAddress = hostAddress;
        this.clientRequestManager = clientRequestManager;

        locker = new ReentrantLock();
        condition = locker.newCondition();
    }

    @Override
    public void open() throws ConnectionFailException {
        try {
            channel = AsynchronousSocketChannel.open();
            Future future = channel.connect(hostAddress);
            future.get();

            readHandler = new ClientReadHandler(this);
            writeHandler = new ClientWriteHandler(this);


        } catch (Exception e) {
            LOGGER.error("Error open connection", e);
            throw new ConnectionFailException(e);
        }
    }

    @Override
    public Long getId() {
        return null;
    }

    @Override
    public void close() {
        try {
            channel.close();
            LOGGER.info("Client closed");
        } catch (IOException e) {
            LOGGER.error("Client closing error", e);
        }
    }

    @Override
    public void send(String message) {
        checkArgument(!Strings.isNullOrEmpty(message));

        if (!channel.isOpen()) {
            return;
        }
        LOGGER.debug("ClientWrite: " + message);

        channel.write(ByteBuffer.wrap(message.getBytes()), getReadBuffer(), getOnWrite());
    }

    @Override
    public void read() {
        if (!channel.isOpen()) {
            return;
        }

        try {
            locker.lock();

            getChannel().read(inputBuffer, inputBuffer, getOnRead());

        } catch (Exception e) {
            LOGGER.error("Connection read error", e);
        } finally {
            if (locker.isHeldByCurrentThread())
                locker.unlock();
        }

        return;
    }

    @Override
    public boolean isClosed() {
        if (channel == null) {
            return true;
        }

        return !channel.isOpen();
    }

    @Override
    public AsynchronousSocketChannel getChannel() {
        return channel;
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public Boolean isRegistered() {
        return !Strings.isNullOrEmpty(userName);
    }

    @Override
    public CompletionHandler<Integer, ByteBuffer> getOnRead() {
        return readHandler;
    }

    @Override
    public ByteBuffer getReadBuffer() {
        return inputBuffer;
    }

    @Override
    public CompletionHandler<Integer, ByteBuffer> getOnWrite() {
        return writeHandler;
    }

    @Override
    public ByteBuffer getWriteBuffer() {
        return outputBuffer;
    }

    @Override
    public void addMessageToQueue(String request) {
        try {
            Request req = TransportProtocol.marshall(request);
            if (req instanceof Response) {
                clientRequestManager.acceptResponse((Response) req);
            } else {
                System.out.println(req.getUserName() + ": " + req.getBody());
            }

        } catch (IOException e) {
            LOGGER.error("Add message to queue", e);
        }
    }

    @Override
    public void onReadStart() {
        try {
            locker.lock();
        } catch (Exception e) {
            LOGGER.error("onReadFinish", e);
        }
    }

    @Override
    public void onReadFinish() {
        try {
            if (locker.isHeldByCurrentThread())
                locker.unlock();
        } catch (Exception e) {
            LOGGER.error("onReadFinish", e);
        }
    }
}
