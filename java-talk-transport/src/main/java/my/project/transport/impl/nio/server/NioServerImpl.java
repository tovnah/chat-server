package my.project.transport.impl.nio.server;

import my.project.exeption.ConnectionFailException;
import my.project.exeption.ServerTransportException;
import my.project.protocol.TransportProtocol;
import my.project.transport.Connection;
import my.project.transport.Const;
import my.project.transport.server.Server;
import my.project.transport.server.ServerRequestManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NioServerImpl implements Server {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private ServerSocketChannel serverChannel;
    private static Selector selector;

    private ServerRequestManager messageManager;

    private String buffer = "";

    private ConcurrentHashMap<SocketChannel, Connection> connections = new ConcurrentHashMap<>();

    public NioServerImpl(ServerRequestManager messageManager) throws Exception {
        this.messageManager = messageManager;

        selector = Selector.open();
        serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);
        ServerSocket ss = serverChannel.socket();
        InetSocketAddress address = new InetSocketAddress(Const.port);
        ss.bind(address);
        serverChannel.register(selector, SelectionKey.OP_ACCEPT);
    }

    @Override
    public void start() {
        LOGGER.info("Server start..");

        while (true) {
            try {
                if (selector.select() == 0)
                    continue;

                Set<SelectionKey> set = selector.selectedKeys();
                Iterator<SelectionKey> iterator = set.iterator();

                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();

                    iterator.remove();

                    if (!key.isValid()) {
                        continue;
                    }

                    if (key.isAcceptable()) this.handleAccept(key);
                    if (key.isReadable()) this.handleRead(key);
                }

            } catch (Exception e) {
                LOGGER.error("Server error", e);
            }
        }
    }

    @Override
    public void registerConnection(Connection connection) throws ServerTransportException {

    }

    @Override
    public void removeConnection(Connection connection) throws ServerTransportException {

    }

    @Override
    public ServerRequestManager getMessageManager() {
        return messageManager;
    }


    private void handleAccept(SelectionKey key) throws IOException {
        SocketChannel sc = ((ServerSocketChannel) key.channel()).accept();
        String address = (new StringBuilder(sc.socket().getInetAddress().toString())).append(":").append(sc.socket().getPort()).toString();

        sc.configureBlocking(false);
        sc.register(selector, SelectionKey.OP_READ, address);

        LOGGER.info("Accepted " + address);

        try {
            Connection connection = new NioServerConnectionImpl(sc, selector, messageManager);
            connection.open();
            connections.put(sc, connection);
        } catch (ConnectionFailException e) {
           LOGGER.error("Accept error", e);
        }

    }

    private void handleRead(SelectionKey key) throws IOException {
        LOGGER.info("Read fire ");
        SocketChannel client = (SocketChannel) key.channel();

        Connection connection = connections.get(client);

        try {
            while (true) {
                String request = readRequest(client);
                if (request == null) {
                    key.cancel();
                    break;
                }
//                if (request.startsWith("{")) {
//                    if (request.contains("}")) {
//                        Integer index = request.lastIndexOf('}');
//
//                        String message = request.substring(0, index + 1);
//
//                        if (index < request.length() - 1) {
//                            buffer = request.substring(index + 1, request.length());
//                        } else {
//                            buffer = "";
//                        }
//
//                        connection.addMessageToQueue(TransportProtocol.restoreMessage(message));
//                        buffer = "";
//                        break;
//
//                    } else {
//                        buffer += request;
//                    }
//                } else {
                    if (request.contains("}")) {
                        Integer index = request.lastIndexOf('}');

                        String message = request.substring(0, index + 1);
                        message = buffer + message;

                        if (index < request.length() - 1) {
                            buffer = request.substring(index + 1, request.length());
                        } else {
                            buffer = "";
                        }


                        connection.addMessageToQueue(TransportProtocol.restoreMessage(message));
                        buffer = "";
                        break;

                    } else {
                        buffer += request;
                    }
//                }
            }

        } catch (Exception e) {
            LOGGER.error("Read error", e);
        }
    }


    public String readRequest(SocketChannel sc) throws Exception {

        ByteBuffer buffer = ByteBuffer.allocate(Const.bufferSize);
        int numRead = sc.read(buffer);
        if (numRead == -1) {
            return null;
        }
        if (numRead == 0) {
            return "";
        }
        byte[] data = new byte[numRead];

        System.arraycopy(buffer.array(), 0, data, 0, numRead);
        return new String(data);
    }
}
