package my.project.transport.impl.aio.server;


import com.google.common.base.Strings;
import my.project.exeption.ConnectionFailException;
import my.project.transport.AsyncSocketConnection;
import my.project.transport.Configuration;
import my.project.transport.Connection;
import my.project.transport.server.ServerRequestManager;
import my.project.protocol.TransportProtocol;
import my.project.transport.impl.aio.server.handlers.ReadServerHandler;
import my.project.transport.impl.aio.server.handlers.WriteServerHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

import static com.google.common.base.Preconditions.checkArgument;

public class AioServerConnectionImpl implements Connection, AsyncSocketConnection {
    private Logger LOGGER = LoggerFactory.getLogger(AioServerConnectionImpl.class);

    private final Long id;
    private String userName;

    private final AsynchronousSocketChannel channel;

    private CompletionHandler<Integer, ByteBuffer> readHandler;
    private CompletionHandler<Integer, ByteBuffer> writeHandler;

    private ByteBuffer inputBuffer = ByteBuffer.allocate(Configuration.getBufferSize());
    private ByteBuffer outputBuffer = ByteBuffer.allocate(Configuration.getBufferSize());

    private final ServerRequestManager messageManager;

    public AioServerConnectionImpl(AsynchronousSocketChannel channel, Long id, ServerRequestManager messageManager) {
        this.channel = channel;
        this.id = id;
        this.messageManager = messageManager;
    }


    @Override
    public void open() throws ConnectionFailException {
        if( channel == null){
            throw new ConnectionFailException();
        }

        this.readHandler = new ReadServerHandler(this);
        this.writeHandler = new WriteServerHandler(this);

        read();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void close() {
        try {
            if (channel != null && channel.isOpen()) {
                channel.close();
            }

            LOGGER.info("Connection " + getId() + " closing");
        } catch (IOException e) {
            LOGGER.error("Error closing", e);
        }
    }


    @Override
    public void send(String message) {
        checkArgument(!Strings.isNullOrEmpty(message));

        if (!channel.isOpen()) {
            messageManager.unregisterConnection(this);
            return;
        }
        LOGGER.debug("ServerWrite: " + message);
        message = TransportProtocol.prepareMessageToTransfer(message);//message.length() + "!" + message;
        LOGGER.debug("ServerWriteEncode: " + message);
        channel.write(ByteBuffer.wrap(message.getBytes()), getReadBuffer(), getOnWrite());
    }

    @Override
    public void read() {
        getChannel().read(inputBuffer, inputBuffer, getOnRead());
    }

    @Override
    public boolean isClosed() {
        if (channel == null) {
            return true;
        }

        return !channel.isOpen();
    }

    @Override
    public AsynchronousSocketChannel getChannel() {
        return channel;
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public Boolean isRegistered() {
        return userName != null && !userName.isEmpty();
    }

    @Override
    public CompletionHandler<Integer, ByteBuffer> getOnRead() {
        return readHandler;
    }

    @Override
    public ByteBuffer getReadBuffer() {
        return inputBuffer;
    }

    @Override
    public CompletionHandler<Integer, ByteBuffer> getOnWrite() {
        return writeHandler;
    }

    @Override
    public ByteBuffer getWriteBuffer() {
        return outputBuffer;
    }

    @Override
    public void addMessageToQueue(String request) {
        LOGGER.debug("Message to queue: " + request);
        messageManager.acceptRequest(request, this);
    }

    @Override
    public void onReadFinish() {

    }

    @Override
    public void onReadStart() {

    }

}
