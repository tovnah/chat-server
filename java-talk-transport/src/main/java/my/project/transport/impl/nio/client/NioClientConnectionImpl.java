package my.project.transport.impl.nio.client;

import com.google.common.base.Strings;
import my.project.exeption.ConnectionFailException;
import my.project.protocol.MessageImpl;
import my.project.protocol.Request;
import my.project.protocol.Response;
import my.project.protocol.TransportProtocol;
import my.project.transport.Connection;
import my.project.transport.NioSocketConnection;
import my.project.transport.client.ClientRequestManager;
import my.project.transport.impl.aio.client.AioClientConnectionImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import static com.google.common.base.Preconditions.checkArgument;

public class NioClientConnectionImpl implements Connection, NioSocketConnection {
    private Logger LOGGER = LoggerFactory.getLogger(NioClientConnectionImpl.class);

    private String userName;
    private SocketChannel channel;
    private InetSocketAddress hostAddress;
    private ClientRequestManager clientRequestManager;

    public NioClientConnectionImpl(InetSocketAddress hostAddress, ClientRequestManager clientRequestManager) {
        this.hostAddress = hostAddress;
        this.clientRequestManager = clientRequestManager;
    }

    @Override
    public void open() throws ConnectionFailException {
        try {
            channel = SocketChannel.open(hostAddress);
        } catch (Exception e) {
            throw new ConnectionFailException();
        }

    }

    @Override
    public Long getId() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void close() {
        if (channel == null) {
            return;
        }

        try {
            channel.close();
        } catch (IOException e) {
            LOGGER.error("Connection close error", e);
        }
    }

    @Override
    public void send(String message) throws IllegalArgumentException {
        checkArgument(!Strings.isNullOrEmpty(message));

        ByteBuffer buffer = ByteBuffer.wrap(TransportProtocol.prepareMessageToTransfer(message).getBytes());

        try {
            channel.write(buffer);
        } catch (IOException e) {
            LOGGER.error("Send message error", e);
        } finally {
            buffer.clear();
        }
    }

    @Override
    public void read() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isClosed() {
        if (channel == null) {
            return true;
        }
        return !channel.isOpen();
    }


    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public Boolean isRegistered() {
        return !Strings.isNullOrEmpty(userName);
    }


    @Override
    public void addMessageToQueue(String request) {
        try {
            Request req = TransportProtocol.marshall(request);
            if (req instanceof Response) {
                clientRequestManager.acceptResponse((Response) req);
            } else {
                System.out.println(req.getUserName() + ": " + req.getBody());
            }

        } catch (IOException e) {
            LOGGER.error("Add message to queue", e);
        }
    }

    @Override
    public void onReadFinish() {

    }

    @Override
    public void onReadStart() {

    }

    @Override
    public SocketChannel getChannel() {
        return channel;
    }
}
