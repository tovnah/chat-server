package my.project.transport;

import java.nio.charset.Charset;

public class Const {
    public static final String host = "localhost";
    public static final int port = 3345;
    public static final int bufferSize = 32;
    public static final Charset charset = Charset.forName("UTF-8");
    public static final int historyLenght = 100;
}
