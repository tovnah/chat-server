package my.project.transport;

import java.nio.channels.SocketChannel;

public interface NioSocketConnection {
    SocketChannel getChannel();
}
