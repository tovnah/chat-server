package my.project.transport.client;

import my.project.protocol.Request;
import my.project.protocol.Response;

public interface ClientRequestManager {
    Response sendAndRead(Request request) throws NullPointerException, IllegalArgumentException;

    void acceptResponse(Response response) throws NullPointerException, IllegalArgumentException;

    void close();

    void init();
}
