package my.project.presentation;

import my.project.protocol.MessageImpl;
import my.project.transport.impl.nio.client.NioClientConnectionImpl;
import my.project.transport.server.ServerRequestManager;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RequestManagerImplTest {

    private ServerRequestManager getServerRequestManager() {
        return new RequestManagerImpl();
    }

    @Test
    public void accept_request_must_thrown_npe_with_nullable_args() {
        assertThrows(NullPointerException.class, () -> {
            getServerRequestManager().acceptRequest(null, new NioClientConnectionImpl(null, null));
        });

        assertThrows(NullPointerException.class, () -> {
            getServerRequestManager().acceptRequest("qwe", null);
        });
    }

    @Test
    public void broadcast_must_thrown_npe_with_nullable_args() {
        assertThrows(NullPointerException.class, () -> {
            getServerRequestManager().broadcast(null, new NioClientConnectionImpl(null, null));
        });

        assertThrows(NullPointerException.class, () -> {
            getServerRequestManager().broadcast(new MessageImpl(), null);
        });
    }

    @Test
    public void registerConnection_must_thrown_npe_with_nullable_args() {
        assertThrows(NullPointerException.class, () -> {
            getServerRequestManager().registerConnection(null);
        });
    }

    @Test
    public void unregisterConnection_must_thrown_npe_with_nullable_args() {
        assertThrows(NullPointerException.class, () -> {
            getServerRequestManager().unregisterConnection(null);
        });
    }

    @Test
    public void onlyAuthorAnswer_must_thrown_npe_with_nullable_args() {
        assertThrows(NullPointerException.class, () -> {
            getServerRequestManager().onlyAuthorAnswer(null, null);
        });

        assertThrows(NullPointerException.class, () -> {
            getServerRequestManager().onlyAuthorAnswer(null, new NioClientConnectionImpl(null, null));
        });

        assertThrows(NullPointerException.class, () -> {
            getServerRequestManager().onlyAuthorAnswer(new MessageImpl(), null);
        });
    }
}