package my.project.presentation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import my.project.protocol.Request;
import my.project.transport.Connection;
import my.project.transport.Const;
import my.project.transport.server.ServerRequestManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import static com.google.common.base.Preconditions.checkNotNull;

public class RequestManagerImpl implements ServerRequestManager {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(1);
    private ConcurrentHashMap<String, Connection> connections = new ConcurrentHashMap<>();
    private Queue<Request> requests = new LinkedBlockingQueue<>(Const.historyLenght);

    private AtomicInteger reqCount = new AtomicInteger(0);
    private AtomicInteger userCount = new AtomicInteger(0);

    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    @Override
    public void acceptRequest(String req, Connection connection) throws NullPointerException {
        checkNotNull(req);
        checkNotNull(connection);

        try {
            LOGGER.info("Accept request " + req);
            Request request = marshall(req);

            threadPoolExecutor.execute(new ServerMessageProcessor(connection, this, request));

        } catch (IOException e) {
            LOGGER.error("MessageManagerImpl error", e);
        }
    }

    @Override
    public void broadcast(Request request, Connection connection) throws NullPointerException {
        checkNotNull(request);
        checkNotNull(connection);

        if (requests.size() >= Const.historyLenght) {
            requests.poll();
        }

        LOGGER.debug("Request count  " + reqCount.incrementAndGet());

        requests.add(request);

        for (Connection client : connections.values()) {
            if (Objects.equals(client.getUserName(), connection.getUserName())) {
                continue;
            }

            try {
                LOGGER.debug("broadcast " + request + " to " + client.getUserName());
                client.send(unmarshall(request));
            } catch (JsonProcessingException e) {
                LOGGER.error("Send message error ", e);
            }
        }
    }

    @Override
    public List<Request> getLastRequests() {
        return Arrays.asList(requests.toArray(new Request[0]));
    }

    @Override
    public Connection getConnectionByName(String name) {
        return connections.get(name);
    }

    @Override
    public void registerConnection(Connection connection) throws NullPointerException {
        checkNotNull(connection);

        LOGGER.debug("User count  " + userCount.incrementAndGet());

        LOGGER.debug("Registered connection " + connection.getUserName());
        connections.put(connection.getUserName(), connection);
    }

    @Override
    public void unregisterConnection(Connection connection) throws NullPointerException{
        checkNotNull(connection);

        LOGGER.debug("Unregistered connection " + connection.getUserName());
        connections.remove(connection.getUserName());
    }

    @Override
    public void onlyAuthorAnswer(Request request, Connection connection) throws NullPointerException{
        checkNotNull(request);
        checkNotNull(connection);

        LOGGER.debug("Answer " + request + " to connection " + connection.getUserName());
        try {
            connection.send(unmarshall(request));
        } catch (JsonProcessingException e) {
            LOGGER.error("Send message error", e);
        }
    }

    @Override
    public void closeConnection(Connection connection) {
        if (connection != null && connection.getUserName() != null) {

            LOGGER.debug("Close connection " + connection.getUserName());

            connections.remove(connection.getUserName());
            connection.close();
        }
    }

    private Request marshall(String request) throws IOException {
        return MAPPER.readValue(request, Request.class);
    }

    private String unmarshall(Request request) throws JsonProcessingException {
        return MAPPER.writeValueAsString(request);
    }
}
