package my.project;

import com.google.common.base.Strings;
import my.project.presentation.ClientRequestManagerImpl;
import my.project.protocol.MessageImpl;
import my.project.protocol.Request;
import my.project.transport.Connection;
import my.project.transport.Const;
import my.project.transport.impl.aio.client.AioClientConnectionImpl;
import my.project.transport.impl.nio.client.NioClientConnectionImpl;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Bart {
    private final String prefix = "client";
    private final Integer count = 1000;
    ExecutorService executorService = Executors.newFixedThreadPool(1000);

    private ConcurrentHashMap<String, Connection> connections = new ConcurrentHashMap<>();

    public void start() {
        createConnections();
    }

    private static List<String> phrases = new ArrayList<>();


    private Boolean createConnections() {

        for (Integer i = 0; i < count; i++) {
            String login = prefix + i;
            ClientRequestManagerImpl messageProcessor = new ClientRequestManagerImpl();
            Connection connection = new NioClientConnectionImpl(new InetSocketAddress(Const.host, Const.port), messageProcessor);

            try {
                connection.open();
            } catch (Exception e) {

            }

            connection.setUserName(login);

            messageProcessor.setConnection(connection);
            messageProcessor.init();
            connections.put(login, connection);

            BotConn botConn = new BotConn(connection, messageProcessor);
            executorService.execute(botConn);
        }

        return true;
    }

    public static class BotConn implements Runnable {
        Connection connection;
        ClientRequestManagerImpl messageProcessor;

        public BotConn(Connection connection, ClientRequestManagerImpl messageProcessor) {
            this.connection = connection;
            this.messageProcessor = messageProcessor;
        }

        @Override
        public void run() {
            try {
                Request request = new MessageImpl(connection.getUserName(), connection.getUserName());
                messageProcessor.sendAndRead(request);

                for (Integer i = 0; i < 300; i++) {
                    request = new MessageImpl(connection.getUserName(), Thread.currentThread().getName() + ": " + getPhrase() + i);
                    messageProcessor.sendAndRead(request);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private String getPhrase() {
            return phrases.get(new Random().nextInt(phrases.size() - 1));
        }
    }

    static {
        phrases.add("I will not instigate revolution.");
        phrases.add("I will not draw naked ladies in class.");
        phrases.add("I did not see Elvis.");
        phrases.add("I will not call my teacher 'Hot Cakes'.");
        phrases.add("Garlic gum is not funny.");
        phrases.add("They are laughing at me, not with me.");
        phrases.add("I will not yell 'Fire' in a crowded classroom. ");
        phrases.add("I will not encourage others to fly.");
        phrases.add("Tar is not a plaything.");
        phrases.add("I will not Xerox my butt.");
        phrases.add("I will not trade pants with others.");
        phrases.add("I am not a 32 year old woman.");
        phrases.add("I will not do that thing with my tongue.");
        phrases.add("I will not drive the principal's car.");
        phrases.add("I will not pledge allegiance to Bart.");
        phrases.add("I will not sell school property.");
        phrases.add("I will not cut corners.");
        phrases.add("I will not get very far with this attitude.");
        phrases.add("I will not make flatulent noises in class.");
        phrases.add("I will not belch the national anthem.");
        phrases.add("I will not sell land in Florida.");
        phrases.add("I will not sell school property.");
        phrases.add("I will not grease the monkey bars.");
        phrases.add("I will not hide behind the Fifth Amendment.");
        phrases.add("I will not do anything bad ever again.");
        phrases.add("I will not show off.");
        phrases.add("I will not sleep through my education.");
    }
}
