package my.project;

import my.project.presentation.ClientUI;
import org.apache.log4j.BasicConfigurator;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<String> keys = Arrays.asList(args);

        if (keys.contains("botMode")) {
            BasicConfigurator.configure();
            Bart bot = new Bart();
            bot.start();
        } else {
            BasicConfigurator.configure();
            ClientUI clientUI = new ClientUI();
            clientUI.start();
        }
    }
}
