package my.project.presentation;

import my.project.protocol.MessageImpl;
import my.project.protocol.Response;
import my.project.transport.Connection;
import my.project.transport.client.ClientRequestManager;
import my.project.transport.impl.nio.client.NioClientConnectionImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ClientRequestManagerImplTest {

    @Test
    public void saveAndRead_must_thrown_on_nullable_request() {
        ClientRequestManager clientRequestManager = new ClientRequestManagerImpl();

        Throwable thrown = assertThrows(NullPointerException.class, () -> {
            clientRequestManager.sendAndRead(null);
        });
    }

    @Test
    public void saveAndRead_must_thrown_on_nullable_connection() {
        ClientRequestManager clientRequestManager = new ClientRequestManagerImpl();

        Throwable thrown = assertThrows(NullPointerException.class, () -> {
            clientRequestManager.sendAndRead(new MessageImpl());
        });
    }

    @Test
    public void saveAndRead_must_thrown_on_nullable_requestId() {
        ClientRequestManagerImpl clientRequestManager = new ClientRequestManagerImpl();
        Connection connection = new NioClientConnectionImpl(null, null);
        clientRequestManager.setConnection(connection);

        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> {
            clientRequestManager.sendAndRead(new MessageImpl());
        });
    }

    @Test
    public void accept_response_must_be_throw_on_nullable_response(){
        ClientRequestManagerImpl clientRequestManager = new ClientRequestManagerImpl();

        Throwable thrown = assertThrows(NullPointerException.class, () -> {
            clientRequestManager.acceptResponse(null);
        });
    }

    @Test
    public void accept_response_must_be_throw_on_nullable_responseId(){
        ClientRequestManagerImpl clientRequestManager = new ClientRequestManagerImpl();

        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> {
            clientRequestManager.acceptResponse(new Response());
        });
    }
}